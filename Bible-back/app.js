/*
 * @Author: jiangh1 1141429595@qq.com
 * @Date: 2022-04-26 09:27:08
 * @LastEditors: jiangh1 1141429595@qq.com
 * @LastEditTime: 2022-10-20 09:25:43
 * @FilePath: \biblical-reptile\shengjinghouduan\app.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// 导入koa
const Koa = require('koa')
const bodyParser = require('koa-bodyparser');
const router = require('./router/index')

const swagger = require('./swagger')  // 存放swagger.js的位置，可以自行配置，我放在了根目录
const { koaSwagger } = require('koa2-swagger-ui')


// 创建一个koa对象
const app = new Koa()

app.use(bodyParser());//中间件
app.use(router.routes()); //路由

// 接口文档配置
app.use(swagger.routes(), swagger.allowedMethods())
app.use(koaSwagger({
  routePrefix: '/swagger', // 接口文档访问地址
  swaggerOptions: {
    url: '/swagger.json', // example path to json 其实就是之后swagger-jsdoc生成的文档地址
  }
}))

//监听端口
const port = 3000
app.listen(port);
console.log(`启动成功,服务端口为:${port}`)
