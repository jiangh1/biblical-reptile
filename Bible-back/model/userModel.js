/*
 * @Date: 2022-04-25 11:10:16
 * @LastEditors: jiangh1 1141429595@qq.com
 * @LastEditTime: 2022-10-13 09:53:14
 * @FilePath: \biblical-reptile\shengjinghouduan\model\userModel.js
 */
const query = require('../sql/query.js')
class UserModel {
    //获取用户
    async getUser(name) {
        return await query(`SELECT * FROM user WHERE name = '${name}'`)
    }

    //获取用户手机号
    async getTel(tel) {
        return await query(`SELECT * FROM user WHERE tel = '${tel}'`)
    }

    //用户注册
    async insert(name, tel, password) {
        return await query(`INSERT INTO user(name, tel, password) VALUES('${name}', '${tel}', '${password}')`)
    }

    //查询目录
    async menu() {
        return await query(`SELECT * FROM menu`)
    }

    //查询一级目录下的二级目录
    async secondlevelmenu(menuId) {
        return await query(`SELECT * FROM secondlevelmenu WHERE menu_id  = '${menuId}'`)
    }

    //查询文章
    async article(menuId, secondlevelmenuId) {
        return await query(`
                    SELECT * FROM article WHERE menu_id = '${menuId}'
                    AND secondlevelmenu_id = '${secondlevelmenuId}'
                    `)

    }

    async create_token(leng) {
        leng = leng == undefined ? 32 : leng	//如果没设置token长度自动为32位
        //预设随机字符串
        let chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz23456789'
        let token = ''
        //以长度单位进行循环
        for (let i = 0; i < leng; ++i) {
            token += chars.charAt(Math.floor(Math.random() * chars.length))
        }
        return await token	///返回之前使用md5加密一下
    }
}
module.exports = new UserModel()