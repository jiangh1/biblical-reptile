/*
 * @Author: jiangh1 1141429595@qq.com
 * @Date: 2022-04-26 09:27:08
 * @LastEditors: jiangh1 1141429595@qq.com
 * @LastEditTime: 2022-10-20 09:43:35
 * @FilePath: \biblical-reptile\shengjinghouduan\router\index.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
const Router = require('koa-router')
const router = new Router()

//用户
const UserController = require('../controller/userController')

//用户注册

// /**
//  * @swagger
//  * /register:
//  *   post:
//  *     summary: 用户注册
//  *     description: 用户注册
//  *     tags:
//  *       - blogs
//  *     parameters:
//  *       - name: author
//  *         in: query
//  *         required: false
//  *         description: 作者
//  *         type: string
//  *       - name: keyword
//  *         in: query
//  *         required: false
//  *         description: 搜索关键字
//  *         type: string
//  *     responses:
//  *       200:
//  *         description: 成功获取
//  */

router.post('/register', UserController.register)

/**
 * @swagger
 * /login:
 *   post:
 *     summary: 用户信息登录
 *     description: 用户信息登录
 *     tags:
 *       - blogs
 *     parameters:
 *       - name: author
 *         in: query
 *         required: false
 *         description: 作者
 *         type: string
 *       - name: keyword
 *         in: query
 *         required: false
 *         description: 搜索关键字
 *         type: string
 *     responses:
 *       200:
 *         description: 成功获取
 */

//用户信息登录
router.post('/login', UserController.login)

//目录查询
router.post('/menu', UserController.menu)

//二级目录查询
router.post('/secondlevelmenu', UserController.secondlevelmenu)

// 文章查询
router.post('/article', UserController.article)


module.exports = router