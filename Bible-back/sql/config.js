/*
 * @Date: 2022-04-25 11:10:16
 * @LastEditors: jiangh1 1141429595@qq.com
 * @LastEditTime: 2022-10-10 11:34:18
 * @FilePath: \biblical-reptile\shengjinghouduan\sql\config.js
 */
const mysqlConfig = {
    user: "", //账号
    password: "", //密码
    database: "", //数据库
    host: "", //服务器地址
    port: 3306, //数据库端口
};
module.exports = mysqlConfig;