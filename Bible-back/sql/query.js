/*
 * @Date: 2022-04-25 11:10:16
 * @LastEditors: jiangh
 * @LastEditTime: 2022-04-25 13:24:05
 * @FilePath: \note\koa2-sql-master\sql\query.js
 */
const mysql = require("mysql");
const config = require("./config");

//创建连接池
const pool = mysql.createPool(config);
const query = (sql, val) => {

    return new Promise((resolve, reject) => {
        pool.getConnection(function(err, connection) {
            if (err) {
                reject(err);
            } else {
                connection.query(sql, val, (err, fields) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(fields);
                    }
                    connection.release();
                });
            }
        });
    });
};

module.exports = query;