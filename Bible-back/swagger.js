/*
 * @Author: jiangh1 1141429595@qq.com
 * @Date: 2022-10-20 09:24:49
 * @LastEditors: jiangh1 1141429595@qq.com
 * @LastEditTime: 2022-10-20 09:36:21
 * @FilePath: \biblical-reptile\shengjinghouduan\swagger.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
const router = require('koa-router')() //引入路由函数
const swaggerJSDoc = require('swagger-jsdoc')
const path = require('path')
const swaggerDefinition = {
    info: {
        title: 'blog项目访问地址',
        version: '1.0.0',
        description: 'API',
    },
    host: 'localhost:3000',// 想着改这里，如果不修改，那么接口文档访问地址为：localhost:8000/swagger
    basePath: '/' // Base path (optional)
};
const options = {
    swaggerDefinition,
    apis: [path.join(__dirname, './router/index.js')], // 写有注解的router的存放地址, 最好path.join()
};
const swaggerSpec = swaggerJSDoc(options)
// 通过路由获取生成的注解文件
router.get('/swagger.json', async function (ctx) {
    ctx.set('Content-Type', 'application/json');
    ctx.body = swaggerSpec;
})
module.exports = router