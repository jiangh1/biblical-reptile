import Vue from 'vue'
import App from './App'
// import Http from './common/service/export.js';
Vue.config.productionTip = false;
// import store from './store'


import uView from "uview-ui";
Vue.use(uView);


// Vue.prototype.$http = Http;
App.mpType = 'app'

const app = new Vue({
    // store,
    ...App
})
app.$mount()