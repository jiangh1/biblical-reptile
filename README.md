<!--
 * @Date: 2022-04-26 09:23:48
 * @LastEditors: 江浩 1141429595@qq.com
 * @LastEditTime: 2022-10-13 19:54:28
 * @FilePath: \biblical-reptile\README.md
-->
# 圣经爬虫

#### 介绍
1.原因：

老娘出去看书不方便，便有了写这个APP的想法，使用爬虫爬到本地，使用JSON数据格式存储

学习爬虫写的一个程序，给老娘写的，郑重声明本人是无神论者

2.项目难点：

主要难度在于python JSON数据处理和对python语言的不熟悉，数据处理好了就容易很多，前端使用uniapp架构页面

3.后期优化问题：

增加阅读历史功能

增加笔记功能

个人页面需完善


优化项目ui，本人审美能力有限

增加搜索功能

文字转语音
    这个我前端要把文章中英文区别出来，然后只读中文部分


#### 软件架构
shenjingGai.py为爬取文章的爬虫代码
sjsy.py为爬取目录的代码
shengjing文件夹为uniapp项目代码

#### 安装教程

1.  打开项目shengjing文件夹
2.  npm i 
3.  使用HBuilder X 运行

ps:数据已爬取 ，存放在shengjing/static/json,无需再度爬取

#### 项目更新
2022.05.07
新增node 接口,数据库
新版本将使用 前后端配合 的方式 来构建 项目 

2022.10.13
实际走通线上流程，postman掉通 post接口 可访问线上数据库

将会保留老旧方式 

#### uniapp 项目截图，第一版
![avatar](/img/1.png)

![avatar](/img/2.png)

![avatar](/img/3.png)

![avatar](/img/4.png)




